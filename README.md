# Priceline

To start run `mix phx.server` and navigate to `localhost:4000/graphiql`.

To run a test query try:

```
query {
  places {
    id
    name
    location
  }
}
```

Or:

```
query {
  place(id: 2) {
    name
    location
    maxGuests
  }
}
```
