defmodule Priceline.Repo do
  use Ecto.Repo,
    otp_app: :priceline,
    adapter: Ecto.Adapters.Postgres
end
