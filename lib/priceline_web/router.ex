defmodule PricelineWeb.Router do
  use PricelineWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :api

    forward "/api", Absinthe.Plug, schema: PricelineWeb.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: PricelineWeb.Schema,
      interface: :simple
  end
end
