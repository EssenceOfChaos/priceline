defmodule PricelineWeb.Resolvers.Vacation do
  @moduledoc """
  Resolves data requested by the GraphQL API from the Ecto Repo
  """
  alias Priceline.Vacation

  def places(_, _, _) do
    {:ok, Vacation.list_places()}
  end

  def place(_, %{id: id}, _) do
    {:ok, Vacation.get_place!(id)}
  end
end
